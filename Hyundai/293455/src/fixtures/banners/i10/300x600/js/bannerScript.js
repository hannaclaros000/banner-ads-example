/*
// FOR BUILT IN DoubleClick
var Enabler;

window.onload = checkDCEnabler;

function checkDCEnabler() {
    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }
}

function enablerInitHandler() {
    document.getElementById('bg-exit').addEventListener('click', bgExitHandler, false);
    init();
}

function bgExitHandler() {
    Enabler.exitOverride("clickThrough", clickTag);
}

*/



/* Please do not modify the code above */

/**
 * Declare local variables here
 */




/**
 * init
 *
 * Where you start your animation and
 * initialisation of your animation.
 * e.g hide the frames.
 *
 */




 // ---------------------------------------------------------------
 // ---------------------------------------------------------------
 // ---------------------------------------------------------------
 // ---------------------------------------------------------------
 // ---------------------------------------------------------------
// WITHOUT DoubleClick



"use strict";
window.onload = init;

/**
 * Declare local variables here
 */

var speedFadeIn = .5,
    speedFadeOut = .5,
    delayTime = .2,
    delayNext = 5,

    delayEnd = 5,
    timer = 0,
    loop = 1,
    bannerWidth = 300,
    bannerHeight = 250;

/**
 * init
 *
 * Where you start your animation and
 * initialisation of your animation.
 * e.g hide the frames.
 *
 */

function init () {
  document.getElementById('bg-exit').addEventListener('click', function () {
      window.open(clickTag)
  });
  TweenMax.to("#covering-div", 1, {alpha: 0});
  ani_01();
}

function ani_01() {
  // fade in
  TweenMax.to( "#top2014", 0, { alpha: 1 } );
  TweenMax.to( "#bottom2014", 0, { alpha: 1 } );

  TweenMax.to( "#overlayWhite", speedFadeIn, { alpha:0, ease:Sine.easeOut } );

  // prep for 2015
  TweenMax.to( "#bottom2015", 0, { alpha:1, rotationX:-90, transformOrigin:"center bottom", transformPerspective:512 } );

  TweenMax.delayedCall( 2, ani_02 );
}

function ani_02() {
  // flip
  TweenMax.to( "#top2015", speedFadeIn, { alpha: 1, ease: Sine.easeInOut } );

  TweenMax.to( "#top2014", .25, { rotationX:-90, transformOrigin:"center bottom", transformPerspective:512 } );
  TweenMax.to( "#bottom2015", 1, { delay:.3, rotationX:-180, transformOrigin:"center bottom", transformPerspective:512 } );

  // prep for 2016
  TweenMax.to( "#bottom2016", 0, { alpha:1, rotationX:-90, transformOrigin:"center bottom", transformPerspective:512 } );

  TweenMax.delayedCall( 2, ani_03 );
}

function ani_03() {
  TweenMax.to( "#top2016", speedFadeIn, { alpha: 1, ease: Sine.easeInOut } );
  //
  TweenMax.to( "#top2015", .25, { rotationX:-90, transformOrigin:"center bottom", transformPerspective:512 } );
  TweenMax.to( "#bottom2016", 1, { delay:.3, rotationX:-180, transformOrigin:"center bottom", transformPerspective:512 } );

  TweenMax.delayedCall( 2, ani_04 );
}

function ani_04()
{
  // fade out calendar etc
  TweenMax.to( "#overlayWhite", speedFadeIn, { alpha:1, ease:Sine.easeOut } );

  // fade in
  var delayTime = .05;

  TweenMax.to( "#txt0401", speedFadeIn, { delay: speedFadeIn, alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0402", speedFadeIn, { delay: speedFadeIn + delayTime, alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0403", speedFadeIn, { delay: speedFadeIn + ( delayTime * 2 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0404", speedFadeIn, { delay: speedFadeIn + ( delayTime * 3 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0405", speedFadeIn, { delay: speedFadeIn + ( delayTime * 4 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0406", speedFadeIn, { delay: speedFadeIn + ( delayTime * 5 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0407", speedFadeIn, { delay: speedFadeIn + ( delayTime * 6 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0408", speedFadeIn, { delay: speedFadeIn + ( delayTime * 7 ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0409", speedFadeIn, { delay: speedFadeIn + ( delayTime * 8 ), alpha:1, ease: Sine.easeInOut } );

  TweenMax.delayedCall( 3, ani_05 );
}

function ani_05()
{
  // fade out
  var delayTime = .05;

  TweenMax.to( "#txt0401", speedFadeOut, { alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0402", speedFadeOut, { delay: delayTime, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0403", speedFadeOut, { delay: delayTime * 2, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0404", speedFadeOut, { delay: delayTime * 3, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0405", speedFadeOut, { delay: delayTime * 4, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0406", speedFadeOut, { delay: delayTime * 5, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0407", speedFadeOut, { delay: delayTime * 6, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0408", speedFadeOut, { delay: delayTime * 7, alpha:0, ease: Sine.easeInOut } );
  TweenMax.to( "#txt0409", speedFadeOut, { delay: delayTime * 8, alpha:0, ease: Sine.easeInOut } );

  // fade in
  var delayTime = .1;

  TweenMax.to( "#car", speedFadeIn, { delay: .7 + delayTime, alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#txt05", speedFadeIn, { delay: .7 + ( 2 * delayTime ), alpha:1, ease: Sine.easeInOut } );
  TweenMax.to( "#cta", speedFadeIn * 2, { delay: .7 + ( 3 * delayTime ), alpha:1, ease: Sine.easeInOut } );

  TweenMax.delayedCall( 1, ani_End );
}



function ani_End() {
  // loop
  if(loop == 1) {
    TweenMax.delayedCall( delayEnd, loopAni );
    loop++
  }
}

function loopAni()
{
    setup();
    TweenMax.delayedCall( 1, ani_01 );
}

function setup()
{
  TweenMax.to( "#car", speedFadeOut, { alpha:0, ease: Sine.easeIn } );
  TweenMax.to( "#txt05", speedFadeOut, { delay: delayTime / 2, alpha:0, ease: Sine.easeIn } );
  TweenMax.to( "#cta", speedFadeOut, { delay: delayTime, alpha:0, ease: Sine.easeIn } );

  TweenMax.to( "#overlayWhite", speedFadeIn, { alpha:1, ease:Sine.easeOut } );

  TweenMax.to( "#top2014", 0, { rotationX:0, transformOrigin:"center bottom", transformPerspective:512, alpha: 0 } );
  TweenMax.to( "#top2015", 0, { rotationX:0, transformOrigin:"center bottom", transformPerspective:512, alpha: 0 } );

  TweenMax.to( "#bottom2014", 0, { alpha:0 } );
  TweenMax.to( "#bottom2015", 0, { alpha:0 } );
  TweenMax.to( "#top2016", 0, { alpha: 0 } );
  TweenMax.to( "#bottom2016", 0, { alpha: 0 } );
}



// -------------------------------------


// check length of animation
// var counterTotal = 30;  // max length of animation
// var counter = 0;
// var timer = setInterval( function() {
//   console.log( "time passed: " + ( 1 + counter++ ) );
//   checkTime();
// }, 1000 );
//
// function checkTime()
// {
//   if( counter >= counterTotal + 1 ){
//     clearInterval( timer );
//   }
// }
