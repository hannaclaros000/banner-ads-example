"use strict";

var Enabler;

window.onload = enablerInitHandler;

function enablerInitHandler() {
 document.getElementById('bg-exit').addEventListener('click', bgExitHandler, false);
   init();
}

function bgExitHandler() {
   window.open(clickTag);
}

var timeToFadeOn=2,
        timeToFadeOff=0.5,
        delayBetweenFades=1,
        loopCounter=1;

function init () {
  TweenMax.to(document.getElementById("car"), 0, {x: 170}),
  TweenMax.to(document.getElementById("wheel"), 0, {x: 443, y:136}),
  TweenMax.to(document.getElementById("wheel2"), 0, {x: 600, y:136}),
  TweenMax.to(document.getElementById("cta"), 0, {alpha:0}),
    text1()
}


function text1(){
  TweenMax.to(document.getElementById("covering-div"),0,{alpha:0})
  TweenLite.to(document.getElementById("car"), 3, {x: -100, ease:Power2.easeOut}),
  TweenLite.to(document.getElementById("wheel"), 3, {x: 170,rotation:-360, ease:Power2.easeOut }),
  TweenLite.to(document.getElementById("wheel2"), 3, {x: 335,rotation:-360, ease:Power2.easeOut }),
  TweenMax.to(document.getElementById("frame1"),0,{ alpha:1, onComplete: text2})
}
function text2(){
  TweenMax.to(document.getElementById("frame1"),timeToFadeOff,{delay:1, alpha:0})
  TweenMax.to(document.getElementById("frame2"),delayBetweenFades,{delay:2, alpha:1, onComplete: text3})
}
function text3(){
  TweenMax.to(document.getElementById("frame2"),timeToFadeOff,{delay:1, alpha:0})
  TweenLite.to(document.getElementById("car"), timeToFadeOn, {delay:2, x: 0}),
  TweenLite.to(document.getElementById("wheel"), timeToFadeOn, {delay:2, x: 271, rotation:360}),
  TweenLite.to(document.getElementById("wheel2"), timeToFadeOn, {delay:2, x: 434, rotation:360}),
  TweenMax.to(document.getElementById("frame3"),delayBetweenFades,{delay:2, alpha:1, onComplete: text4})
}
function text4(){
  TweenMax.to(document.getElementById("frame3"),timeToFadeOff,{delay:1, alpha:0})
  TweenMax.to(document.getElementById("cta"),delayBetweenFades,{delay:2, alpha:1})
  TweenMax.to(document.getElementById("frame4"),delayBetweenFades,{delay:2, alpha:1, onComplete: text5})
}
function text5(){
  TweenMax.to(document.getElementById("frame4"),delayBetweenFades,{ delay:4, onComplete: loop})
}
function loop() {
    if ( loopCounter < 2 ) {
    TweenMax.to(document.getElementById("frame4"),0,{ alpha:0})
    init()

        loopCounter++;
    }
}
