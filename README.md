
## Get Started

Run the following commands to get started. You will need to have node.js installed on your system - https://nodejs.org/en/

    npm install


Generate the production code

    grunt

#### Once grunt is running the banners should be viewable at this location http://localhost:9000/

