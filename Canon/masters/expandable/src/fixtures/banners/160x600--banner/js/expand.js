function onLoadHandler() {
    console.log("PAGE LOADED"), Enabler.isInitialized() ? enablerInitHandler() : Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler)
}
function enablerInitHandler() {
    console.log("ENABLER INIT"), Enabler.isPageLoaded() ? pageLoadedHandler() : Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler)
}
var fullscreenButton, collapseExit, closeButton, fullscreenExit, supportsTouch;
pageLoadedHandler = function() {
    supportsTouch = "ontouchstart"in window, content = document.getElementById("banner"), fullscreenButton = document.getElementById("fullscreen_btn"), collapseExit = document.getElementById("collapse_exit"), fullscreenContent = document.getElementById("fullBanner"), fullscreenExit = document.getElementById("fullscreen_exit"), closeButton = document.getElementById("close_hotspot"), addListeners(), content.style.display = "block", fullscreenButton.style.display = "block"
}, addListeners = function() {
    closeButton.addEventListener("click", closeButtonHandler, !1), fullscreenExit.addEventListener("click", fullscreenExitHandler, !1), collapseExit.addEventListener("click", collapseExitHandler, !1), Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START, fullscreenExpandStartHandler), Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, fullscreenExpandFinishHandler), Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START, fullscreenCollapseStartHandler), Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH, fullscreenCollapseFinishHandler), fullscreenButton.addEventListener("mouseover", fullscreenHandler, !1)
}, fullscreenHandler = function() {
    Enabler.requestExpand(), setTimeout(function() {
        fullscreenContent.style.display = "block"
    }, 300), Enabler.counter("Full Screen count")
}, fullscreenExpandStartHandler = function() {
    Enabler.finishExpand()
}, fullscreenExpandFinishHandler = function() {
    Enabler.startTimer("Full Screen duration")
}, fullscreenCollapseStartHandler = function() {
    Enabler.finishCollapse()
}, fullscreenCollapseFinishHandler = function() {
    Enabler.stopTimer("Full Screen duration")
}, fullscreenExitHandler = function() {
    Enabler.requestCollapse(), Enabler.exit("HTML5_Background_Clickthrough"), fullscreenContent.style.display = "none"
}, collapseExitHandler = function() {
    Enabler.exit("HTML5_Background_Clickthrough")
}, closeButtonHandler = function() {
    Enabler.reportManualClose(), Enabler.requestCollapse(), fullscreenContent.style.display = "none"
}, window.addEventListener("load", onLoadHandler);
