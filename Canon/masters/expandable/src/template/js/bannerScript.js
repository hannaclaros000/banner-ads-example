"use strict";
function checkDCEnabler() {
    Enabler.isInitialized() ? enablerInitHandler() : Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler)
}
function enablerInitHandler() {
    document.getElementById("bg-exit").addEventListener("click", bgExitHandler, !1), init()
}
function bgExitHandler() {
    Enabler.exit("clickThrough", clickTag)
}
function init() {
    TweenMax.to(document.getElementById("covering-div"), 1, {
        alpha: 0
    }), TweenMax.to(document.getElementById("bg1"), 0, {
        x: 350
    }), TweenMax.to(document.getElementById("bg"), 0, {
        x: - 130
    }), TweenMax.to(document.getElementById("covering-div"), 0, {
        alpha: 0
    }), TweenMax.to(document.getElementById("open"), 0, {
        alpha: 0
    }), TweenMax.to(document.getElementById("openCTA"), 0, {
        alpha: 0
    }), TweenMax.to(document.getElementById("circle"), 0, {
        alpha: 0
    }), text1(), bg()
}
function bg() {
    TweenMax.to(document.getElementById("bg"), 7, {
        delay: 1,
        x: 0,
        ease: Power3.easeOut
    })
}
function text1() {
    TweenMax.to(document.getElementById("covering-div"), timeToFadeOff, {
        alpha: 0
    }), TweenMax.to(document.getElementById("frame1"), timeToFadeOff, {
        delay: 1,
        alpha: 1,
        ease: Power2.easeIn,
        onComplete: text2
    })
}
function text2() {
    TweenMax.to(document.getElementById("frame2"), timeToFadeOff, {
        delay: delayBetweenFades,
        alpha: 1,
        ease: Power2.easeIn,
        onComplete: text3
    })
}
function text3() {
    TweenMax.to(document.getElementById("frame3"), timeToFadeOff, {
        delay: delayBetweenFades,
        alpha: 1,
        ease: Power2.easeIn,
        onComplete: text4
    })
}
function text4() {
    TweenMax.to(document.getElementById("frame4"), timeToFadeOff, {
        delay: delayBetweenFades,
        alpha: 1,
        ease: Power2.easeIn,
        onComplete: text5
    })
}
function text5() {
    TweenMax.to(document.getElementById("frame1"), timeToFadeOff, {
        delay: timeToFadeOn,
        x: - 300,
        ease: Power2.easeIn
    }), TweenMax.to(document.getElementById("frame2"), timeToFadeOff, {
        delay: timeToFadeOn,
        x: - 300,
        ease: Power2.easeIn
    }), TweenMax.to(document.getElementById("frame3"), timeToFadeOff, {
        delay: timeToFadeOn,
        x: - 300,
        ease: Power2.easeIn
    }), TweenMax.to(document.getElementById("frame4"), timeToFadeOff, {
        delay: timeToFadeOn,
        x: - 300,
        ease: Power2.easeIn
    }), TweenMax.to(document.getElementById("bg1"), timeToFadeOff, {
        delay: timeToFadeOn,
        x: 0,
        ease: Power2.easeIn,
        onComplete: text6
    })
}
function text6() {
    TweenMax.to(document.getElementById("open"), timeToFadeOff, {
        delay: delayBetweenFades,
        ease: Power2.easeInOut,
        alpha: 1
    }), TweenMax.to(document.getElementById("openCTA"), timeToFadeOff, {
        delay: delayBetweenFades,
        ease: Power2.easeInOut,
        alpha: 1
    }), TweenMax.to(document.getElementById("circle"), timeToFadeOff, {
        delay: delayBetweenFades,
        ease: Power2.easeInOut,
        alpha: 1
    }), TweenMax.to(document.getElementById("frame5"), timeToFadeOff, {
        delay: delayBetweenFades,
        ease: Power2.easeInOut,
        alpha: 1
    }), TweenMax.to(document.getElementById("frame6"), timeToFadeOff, {
        delay: delayBetweenFades,
        alpha: 1,
        ease: Power2.easeInOut,
        onComplete: loop
    })
}
function loop() {
    3 > loopCounter && (TweenMax.to(document.getElementById("frame5"), 0, {
        delay: 4,
        alpha: 0
    }), TweenMax.to(document.getElementById("bg1"), 0, {
        delay: 4,
        alpha: 0
    }), TweenMax.to(document.getElementById("frame6"), 0, {
        delay: 4,
        alpha: 0,
        onComplete: reset
    }), loopCounter++)
}
function reset() {
    TweenMax.to(document.getElementById("frame1"), 0, {
        x: 0,
        alpha: 0
    }), TweenMax.to(document.getElementById("frame2"), 0, {
        x: 0,
        alpha: 0
    }), TweenMax.to(document.getElementById("frame3"), 0, {
        x: 0,
        alpha: 0
    }), TweenMax.to(document.getElementById("frame4"), 0, {
        x: 0,
        alpha: 0
    }), TweenMax.to(document.getElementById("bg1"), 0, {
        x: 300,
        alpha: 1,
        onComplete: init
    })
}
var Enabler;
window.onload = init;
var timeToFadeOn = 1.25, timeToFadeOff = 1, delayBetweenFades = .5, loopCounter = 0, logoOnStart=!0;
