"use-strict";

function initEB() {
    if (!EB.isInitialized()) {
        EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
    }
    else {
        startAd();
    }
}

function startAd() {
    addEventListeners();
    init(); 
}

function addEventListeners() {
    document.getElementById("bg-exit").addEventListener("click", clickthrough);
}

function clickthrough() {
    EB.clickthrough();
}

window.addEventListener('load', initEB);

function init() {

 TweenMax.set("#frame1", {alpha: 0});
 TweenMax.set("#frame2", {alpha: 0});
 TweenMax.set("#frame3", {alpha: 0});
 TweenMax.set("#frame4", {alpha: 0});
 TweenMax.set("#frame5", {alpha: 1});
 TweenMax.set("#sign", {alpha: 1});
 TweenMax.set("#bg", {alpha:1});
 TweenMax.set("#bg1", {alpha:1});
 TweenMax.set("#bg2", {alpha: 1});
 TweenMax.set("#bg3", {alpha: 1});
 TweenMax.set("#sparkle1_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle1_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle2_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle2_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle3_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle3_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle4_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle4_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle5_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle5_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle6_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle6_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle7_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle7_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle8_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle8_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle9_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle9_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle10_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle10_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle11_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle11_2", {alpha: 0, scale:1});
 TweenMax.set("#sparkle12_1", {alpha: 0, scale:1});
 TweenMax.set("#sparkle12_2", {alpha: 0, scale:1});
 TweenMax.set("#logo", {alpha: 1});
 TweenMax.set("#cta", {alpha: 1});
 ani_01();
 spark01();
 spark02();
}


function spark02(){
var tl = new TimelineMax({repeat:-1});

tl.to("#bg1", 0.8, {alpha: 0,ease: Power2.easeOut})
.to("#bg2", 0.8, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg3", 0.8, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg2", 0.8, {alpha: 1, ease: Power2.easeOut}, "-=0.3")
.to("#bg1", 0.8, {alpha: 1,ease: Power2.easeOut}, "-=0.2")
;
}

function spark01(){
var tl = new TimelineMax({repeat:-1});

tl.to("#sparkle1_1", .1, {alpha: 1,ease: Power2.easeOut}, "-=4.9")
.to("#sparkle1_1", .1, {alpha: 0,ease: Power2.easeOut}, "-=4.7")
.to("#sparkle1_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=5")
.to("#sparkle1_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.8")
.to("#sparkle5_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=5.1")
.to("#sparkle5_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.9")
.to("#sparkle5_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=4.8")
.to("#sparkle5_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.7")
.to("#sparkle2_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=3.9")
.to("#sparkle2_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=3.7")
.to("#sparkle2_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=2.2")
.to("#sparkle2_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=2")
.to("#sparkle3_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=1.5")
.to("#sparkle3_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=1.2")
.to("#sparkle3_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=3")
.to("#sparkle3_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=2.8")
.to("#sparkle4_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=2")
.to("#sparkle4_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=1.8")
.to("#sparkle4_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=1.9")
.to("#sparkle4_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=1.7")
.to("#sparkle6_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=2.8")
.to("#sparkle6_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=2.6")
.to("#sparkle7_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=5.8")
.to("#sparkle7_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=5.6")
.to("#sparkle8_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=4.5")
.to("#sparkle8_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.3")
.to("#sparkle8_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=1.8")
.to("#sparkle8_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=1.7")
.to("#sparkle9_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=3.9")
.to("#sparkle9_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=3.7")
.to("#sparkle9_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=5.9")
.to("#sparkle9_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=5.7")
.to("#sparkle10_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=4.2")
.to("#sparkle10_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=4")
.to("#sparkle10_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=4.6")
.to("#sparkle10_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.4")
.to("#sparkle11_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=2.5")
.to("#sparkle11_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=2.2")
.to("#sparkle11_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=4.8")
.to("#sparkle11_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.6")
.to("#sparkle12_1", .1, {alpha: 1, ease: Power2.easeOut}, "-=5")
.to("#sparkle12_1", .1, {alpha: 0, ease: Power2.easeOut}, "-=4.8")
.to("#sparkle12_2", .1, {alpha: 1, ease: Power2.easeOut}, "-=5.7")
.to("#sparkle12_2", .1, {alpha: 0, ease: Power2.easeOut}, "-=5.5");

}

function ani_01(){
TweenMax.to("#covering-div", 0, {alpha: 0});
TweenMax.to("#frame1",2, {alpha:1, delay:0.8, ease: Power2.easeOut});
TweenMax.to("#frame2",2, {alpha:1, delay:1.2,  ease: Power2.easeOut});
TweenMax.to("#frame3",2, {alpha:1, delay:1.6,  ease: Power2.easeOut});
TweenMax.to("#frame4",2, {alpha:1, delay:2,  ease: Power2.easeOut});
}