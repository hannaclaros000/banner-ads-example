function loadServingSysScript(relativeUrl) {
    document.write("<script src='" + (document.location.protocol === "https:" ? "https://secure-" : "http://") + "ds-cc.serving-sys.com/BurstingScript/ch/" + relativeUrl + "'><\/script>");
}

//Load secure or insecure version of EBLoader
loadServingSysScript("EBLoader.js");
