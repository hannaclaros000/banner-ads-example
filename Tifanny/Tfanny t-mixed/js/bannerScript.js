/** Sizmek API below **/
/* global TweenMax, Power2 */

"use-strict";

function initEB() {
    if (!EB.isInitialized()) {
        EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
    }
    else {
        startAd();
    }
}

function startAd() {
    addEventListeners();
    init(); //function to start gsap animation
}

function addEventListeners() {
    document.getElementById("bg-exit").addEventListener("click", clickthrough);
}

function clickthrough() {
    EB.clickthrough();
}

window.addEventListener('load', initEB);


function init() {

 TweenMax.set("#frame1", {alpha: 0});
 TweenMax.set("#frame2", {alpha: 0});
 TweenMax.set("#frame3", {alpha: 0});
 TweenMax.set("#frame4", {alpha: 0});
 TweenMax.set("#bg1", {alpha:1});
 TweenMax.set("#bg2", {alpha: 1});
 TweenMax.set("#bg3", {alpha: 1});
 TweenMax.set("#bg4", {alpha: 1});
 TweenMax.set("#bg5", {alpha: 1});
 TweenMax.set("#bg6", {alpha: 1});
 TweenMax.set("#bg7", {alpha: 1});
 TweenMax.set("#bg8", {alpha: 1});
 TweenMax.set("#logo", {alpha: 1});
 TweenMax.set("#cta", {alpha: 1});
 ani_01();
 spark01();
}


function spark01(){
var tl = new TimelineMax({repeat:-1});

tl.to("#bg2", .6, {alpha: 0, ease: Power2.easeOut})
.to("#bg3", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg4", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg5", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg6", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg7", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.3")
.to("#bg8", .6, {alpha: 0, ease: Power2.easeOut}, "-=0.2");
}

function ani_01(){
TweenMax.to("#covering-div", 0, {alpha: 0});
TweenMax.to("#frame1",2, {alpha:1, delay:0.8, ease: Power2.easeOut});
TweenMax.to("#frame2",2, {alpha:1, delay:1.2,  ease: Power2.easeOut});
TweenMax.to("#frame3",2, {alpha:1, delay:1.6,  ease: Power2.easeOut});
TweenMax.to("#frame4",2, {alpha:1, delay:2,  ease: Power2.easeOut});
}