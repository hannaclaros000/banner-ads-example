"use strict";

window.onload = initEB;

function initEB() {
  try {
    EB.isInitialized() ? startAd() : EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd)
  } catch (a) {
    startAd();
  }
}

function startAd() {
    addEventListeners();
    init();
    clickthru();
}

function addEventListeners() {
    document.getElementById("bg-exit").addEventListener("click", clickthrough);
}

function clickthrough() {
    EB.clickthrough();
}

function clickthru() {
    document.getElementById("bg-exit").addEventListener("click", function() {
        window.open(clickTag);
    });
}

/**
 * Declare local variables here
 * Read carefully each settings below
 */

var loopCount     = 0, // standard value is 0
    maxLoops      = 2, // 1 for 15secs, 2 for 30secs
    slideAniSpeed = 1, // standard value is 1
    slideWait     = 3.5, // standard value is 3.5
    resetAniSpeed = 0.1, // standard value is 0.1
    squareBanner = true, // true if you're working on 300x50 or similar (new)
    logoOnStart = false, // true if has logo on start (improved)
    changingBG = true, // true if the banner has two backgrounds (new)
    bannerWidth,
    bannerHeight,
    leftSlidePosition,
    rightSlidePosition;

/**
 * init
 *
 * Where you start your animation and
 * initialisation of your animation.
 * e.g hide the frames.
 *
 */

function init() {

    bannerWidth = parseInt(document.getElementById('banner').offsetWidth);
    bannerHeight = parseInt(document.getElementById('banner').offsetHeight);

    leftSlidePosition = (0 - bannerWidth)
    rightSlidePosition = bannerWidth;

    if (logoOnStart) {
        TweenLite.to("#frame5", 0, {opacity: 1});
    } else {
        if (!squareBanner) {
            TweenLite.to("#frame5", 0, {y: 150, opacity: 0});
        }
        TweenLite.to("#frame5", 0, {opacity: 0});
    }

    if (changingBG) {
        TweenLite.to("#background2", 0, {x: rightSlidePosition, opacity: 0});
    }

    TweenLite.to("#frame1", 0, {x: leftSlidePosition, opacity: 0});
    TweenLite.to("#frame2", 0, {x: leftSlidePosition, opacity: 0});
    TweenLite.to("#frame3", 0, {x: leftSlidePosition, opacity: 0});
    TweenLite.to("#frame4", 0, {x: leftSlidePosition, opacity: 0});
    TweenLite.to("#colouredOverlay", 0, {x: 0, y: 0, opacity: 0, onComplete: ani_01});
}

function ani_01() {

    TweenLite.to("#covering-div", 0, {alpha: 0});
    TweenLite.to("#background1", 0, {x: 0, opacity: 1, ease:Linear.easeOutIn });
    TweenLite.to("#frame1", slideAniSpeed, {x: 0, opacity: 1, ease:Linear.easeOutIn });
    TweenLite.delayedCall( slideWait, ani_02 );
}

function ani_02() {

    if (changingBG) {
        TweenLite.to("#background1", slideAniSpeed, {x: leftSlidePosition, ease:Linear.easeOutIn });
        TweenLite.to("#background2", 0, {opacity: 1, ease:Linear.easeOutIn });
        TweenLite.to("#background2", slideAniSpeed, { overwrite:false, x: 0, ease:Linear.easeOutIn });
        TweenLite.to("#frame1", slideAniSpeed, {x: leftSlidePosition});
    } else {
        TweenLite.to("#frame1", slideAniSpeed, {x: rightSlidePosition});
    }

    TweenLite.to("#frame1", slideAniSpeed, {ease:Linear.easeOutIn});
    TweenLite.to("#frame2", slideAniSpeed, {x: 0, delay: 0.7, opacity: 1, ease:Linear.easeOutIn });
    TweenLite.delayedCall( slideWait, ani_03 );
}

function ani_03() {

    TweenLite.to("#frame2", slideAniSpeed, {x: rightSlidePosition, ease:Linear.easeOutIn });
    TweenLite.to("#colouredOverlay", slideAniSpeed, {opacity: .6, ease:Linear.easeOutIn });
    TweenLite.to("#frame3", slideAniSpeed, {x: 0, delay: 0.7, opacity: 1, ease:Linear.easeOutIn });
    TweenLite.to("#frame4", slideAniSpeed, {x: 0, delay: 0.7, opacity: 1, ease:Linear.easeOutIn });

    if (squareBanner) {
        TweenLite.delayedCall( slideWait, ani_04 );
    } else {
        TweenLite.delayedCall( 0, ani_04 );
    } 
}

function ani_04() {

    if (squareBanner) {
        TweenLite.to("#frame5", slideAniSpeed, {opacity: 1, ease:Linear.easeOutIn});
    } else {
        TweenLite.to("#frame5", slideAniSpeed, {opacity: 1, delay: 0.7, y: 0, ease:Linear.easeOutIn });
    }

    // for infinite
    // resetAni();

    // for 15 or 30secs
    TweenLite.delayedCall(5.5, init);
}