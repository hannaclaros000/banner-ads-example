// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google

//Declaring elements from the HTML i.e. Giving them Instance Names like in Flash - makes it easier
var container;
var content;
var bgExit;
var vid1Container;
var vid1;
var vid1PlayBtn;
var vid1PauseBtn;
var vid1UnmuteBtn;
var vid1MuteBtn;
var vid1ReplayBtn;
var vid1StopBtn;
var vid1ClickToPlay;

var videoIsEnded = false;


//Function to run with any animations starting on load, or bringing in images etc
function videoInit(){

	//Assign All the elements to the element on the page
	container = document.getElementById('video_container');
	content = document.getElementById('content_dc');
	bgExit = document.getElementById('background_exit');
	vid1Container = document.getElementById('video_interface');
	vid1 = document.getElementById('video1');
	vid1PlayBtn = document.getElementById('play_btn');
	vid1PauseBtn = document.getElementById('pause_btn');
	vid1UnmuteBtn = document.getElementById('sound_on');
	vid1MuteBtn = document.getElementById('sound_off');
	vid1ReplayBtn = document.getElementById('replay');
	vid1StopBtn = document.getElementById('stop_btn');
	vid1ClickToPlay = document.getElementById('watch_cta');

	//Bring in listeners i.e. if a user clicks or rollsover
	addListeners();
	//Bring in Video tracking and start video
	addVideoTracking();
	//Show Ad
	container.style.display = "block";

}

//Add Event Listeners
addListeners = function() {
	bgExit.addEventListener('click', bgExitHandler, false);
	vid1PlayBtn.addEventListener('click', pausePlayHandler, false);
	vid1PauseBtn.addEventListener('click', pausePlayHandler, false);
	vid1MuteBtn.addEventListener('click', muteUnmuteHandler, false);
	vid1UnmuteBtn.addEventListener('click', muteUnmuteHandler, false);
	vid1ReplayBtn.addEventListener('click', replayHandler, false);
	vid1StopBtn.addEventListener('click', stopHandler,false);
	vid1ClickToPlay.addEventListener('click',playvideoHandler,false);
	vid1.addEventListener('ended', videoEndHandler, false);
}

//Add Video Metrics to the HTML5 Video Elements by Loading in Video Module, and assigning to Videos
addVideoTracking = function() {
	Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
		studio.video.Reporter.attach('video_1', vid1);
	});

	vid1MuteBtn.style.visibility = 'visible';
	vid1UnmuteBtn.style.visibility = 'hidden';
	vid1PlayBtn.style.visibility = 'visible';
	vid1PauseBtn.style.visibility = 'hidden';

		//Ipad, iPod, iPhone exception. Safari on mobile can not use 	mute by controls

	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPod/i)) ){
	document.getElementById("sound_on").style.opacity=0;
	document.getElementById("sound_off").style.opacity=0;
}

}

bgExitHandler = function(e) {
	//Call Exits
	Enabler.exit('HTML5_Background_Clickthrough');
	//Pause Video
	vid1.pause();
	//Setup Buttons
	vid1PauseBtn.style.visibility = 'hidden';
	vid1PlayBtn.style.visibility = 'visible';
}

playvideoHandler = function(e){
	vid1.play();
	if (vid1.volume == 0.0) {
	//If Muted then Turn Volume Back to 1.0
		vid1.volume = 1.0;
		vid1MuteBtn.style.visibility = 'visible';
		vid1UnmuteBtn.style.visibility = 'hidden';
	}
	//show requiredbuttons
	vid1ClickToPlay.style.visibility = 'hidden';
	vid1PlayBtn.style.visibility = 'hidden';
	vid1PauseBtn.style.visibility = 'visible';

	videoIsEnded = false;
}

pausePlayHandler = function(e) {
	vid1ClickToPlay.style.visibility = 'hidden';
	//alert(e);
	if (vid1.paused && videoIsEnded) {
		//If Paused then Play
		vid1.play();
		//Turn sound on
		vid1.volume = 1.0;
		//Show required buttons
		vid1PauseBtn.style.visibility = 'visible';
		vid1PlayBtn.style.visibility = 'hidden';
		vid1MuteBtn.style.visibility = 'visible';

		videoIsEnded = false;
	}else if(vid1.paused){
		//If Paused then Play
		vid1.play();
		//Show Pause button and hide Play button
		vid1PauseBtn.style.visibility = 'visible';
		vid1PlayBtn.style.visibility = 'hidden';

	}else {
		//If not paused then Pause
		vid1.pause();
		//Show Play button and hide Pause button
		vid1PauseBtn.style.visibility = 'hidden';
		vid1PlayBtn.style.visibility = 'visible';
	}
}

muteUnmuteHandler = function(e) {
	if (vid1.volume == 0.0) {
	//If Muted then Turn Volume Back to 1.0
		vid1.volume = 1.0;
		vid1MuteBtn.style.visibility = 'visible';
		vid1UnmuteBtn.style.visibility = 'hidden';
	} else {
	//Otherwise Turn Volume Off
		vid1.volume = 0.0;
		vid1MuteBtn.style.visibility = 'hidden';
		vid1UnmuteBtn.style.visibility = 'visible';
	}
}

replayHandler = function(e) {
	//set video's first frame
	vid1.currentTime = 0;
	//Play film
	vid1.play();
	//Turn sound on
	vid1.volume = 1.0;
	//Show required buttons
	vid1PauseBtn.style.visibility = 'visible';
	vid1MuteBtn.style.visibility = 'visible';
	vid1ClickToPlay.style.visibility = 'hidden';
}

stopHandler = function (e){
	Enabler.counter('EVENT_VIDEO_STOP');
	//set video's first frame
	vid1.currentTime = 0;
	//Pause film
	vid1.pause();

	videoIsEnded = true;
	//Show required buttons
	vid1ClickToPlay.style.visibility = 'visible';
	vid1PauseBtn.style.visibility = 'hidden';
	vid1PlayBtn.style.visibility = 'visible';
}

videoEndHandler = function(e) {
	//set video's first frame
	vid1.currentTime = 0;
	//Pause film
	vid1.pause();

	videoIsEnded = true;

	//show requiredbuttons
	vid1ClickToPlay.style.visibility = 'visible';
	vid1PauseBtn.style.visibility = 'hidden';
	vid1PlayBtn.style.visibility = 'visible';
}
