"use strict";

var Enabler;

window.onload = init;

function init () {
  startAd();
  initVideo();
}

  var hold = 2, fadeIn = 2.5, fadeOut = 1, longFadeIn = 3.5;

  function startAd(){
    TweenLite.to('#covering-div',0,{opacity:1, x:0}),
    TweenLite.to('#highlights',0,{opacity:0}),
    TweenLite.to('#txt1',0,{opacity:0}),
    TweenLite.to('#txt2',0,{opacity:0}),
    TweenLite.to('#txt3',0,{opacity:0}),
    TweenLite.to('#glow',0,{opacity:0}),
    TweenLite.to('#echo_dark',0,{opacity:1}),
    TweenLite.to('#echo',0,{opacity:1, y:0}),
    TweenLite.to('#watch_cta',0,{opacity:0}),
    anim1();
    echoAnim();
  }

  function echoAnim(){
    TweenLite.to('#echo_dark',3,{delay:.5, opacity:0.3,rotationZ: 0.01, ease:Linear.easeNone, force3D:true}),
    TweenLite.to('#echo',7,{delay:1.5, y:157, rotationZ: 0.01, ease:Linear.easeNone, force3D:true})
    TweenLite.to('#echo_dark',7,{delay:1.5, y:157, opacity:0, force3D: true, rotationZ: 0.01, ease:Linear.easeNone})
  }

  function anim1(){
    TweenLite.to('#covering-div',0,{opacity:0, x:400}),
    TweenLite.to('#highlights',0,{opacity:1, onComplete: anim2})
    document.getElementById('watch_cta').style.display = 'block'

  }

  function anim2(){
    TweenLite.to('#txt1',fadeIn,{delay:2, opacity:1}),
    TweenLite.to('#txt2',fadeIn,{delay:4, opacity:1}),
    TweenLite.to('#txt3',fadeIn,{delay:6, opacity:1}),
    TweenLite.to('#watch_cta',fadeIn,{delay:8, opacity:1}),
    TweenLite.to('#glow',fadeIn,{delay:2.5, scale:1, opacity:1})
    document.getElementById('watch_cta').style.display = 'block'

  }

//var cta;
var container;
var btnWrapper;
var bgExit;
var vidBgExit;
var vid1Container;
var vid1;
var vid1PlayBtn;
var vid1PauseBtn;
var vid1UnmuteBtn;
var vid1MuteBtn;
var vid1ReplayBtn;
var vid1StopBtn;
var vid1ClickToPlay;
var closeVid;
var interfaceBg;

var videoIsEnded = false;


//Function to run with any animations starting on load, or bringing in images etc
var initVideo = function (){
    	container = document.getElementById('banner');
      vidBgExit = document.getElementById('background_exit');
      bgExit = document.getElementById('bg-exit');
    	vid1Container = document.getElementById('video_interface');
      btnWrapper = document.getElementById('video_controls');
    	vid1 = document.getElementById('video1');
    	vid1PlayBtn = document.getElementById('play_btn');
    	vid1PauseBtn = document.getElementById('pause_btn');
    	vid1UnmuteBtn = document.getElementById('sound_on');
    	vid1MuteBtn = document.getElementById('sound_off');
    	//vid1ReplayBtn = document.getElementById('replay');
    	vid1StopBtn = document.getElementById('stop_btn');
    	vid1ClickToPlay = document.getElementById('watch_cta');
      closeVid = document.getElementById('close_cta');
      interfaceBg = document.getElementById('interfaceBg');
      //cta = document.getElementById('banner');
    	addListeners();
    	addVideoTracking();
    	container.style.display = "block";
      btnWrapper.style.display = "none";
      vid1ClickToPlay.style.opacity = '0';
    },


    //Add Event Listeners
    addListeners = function() {
      bgExit.addEventListener('click', bgExitHandler, false);
      vidBgExit.addEventListener('click', vidBgExitHandler, false);
    	vid1PlayBtn.addEventListener('click', playvideoHandler, false);
    	vid1PauseBtn.addEventListener('click', pausePlayHandler, false);
    	vid1MuteBtn.addEventListener('click', muteUnmuteHandler, false);
    	vid1UnmuteBtn.addEventListener('click', muteUnmuteHandler, false);
    	//vid1ReplayBtn.addEventListener('click', replayHandler, false);
      vid1StopBtn.addEventListener('click', stopHandler,false);
      closeVid.addEventListener('click', closeVideoHandler,false);

    	vid1ClickToPlay.addEventListener('click',openvideoHandler,false);
    	vid1.addEventListener('ended', videoEndHandler, false);
      //vid1ClickToPlay.addEventListener("mouseover", ctaHandler);

    },


    //opacity = function (){
      //var onCanvas = document.getElementById('watch_cta').style.opacity == '1';
      //if(onCanvas){
        //vid1ClickToPlay.style.opacity = '0.8';
      //}
      //else{
        //vid1ClickToPlay.style.opacity = '0';
      //}
    //},

    bgExitHandler = function(e) {
    	//Call Exits
    	Enabler.exit('Banner_Clickthrough');
      //console.log('Banner CT');
    },

    vidBgExitHandler = function(e) {
    	//Call Exits
    	Enabler.exit('Video_Clickthrough');
    	vid1.pause();
    	vid1PauseBtn.style.visibility = 'hidden';
    	vid1PlayBtn.style.visibility = 'visible';
      //console.log('Video CT');

    },

    openvideoHandler = function(){
      vid1.play();
      vid1.volume = 1.0;
      vid1PauseBtn.style.visibility = 'visible';
      vid1PlayBtn.style.visibility = 'hidden';
      vid1Container.style.visibility = 'visible';
      vid1ClickToPlay.style.visibility = 'hidden';
      vid1MuteBtn.style.visibility = 'visible';
      interfaceBg.style.visibility = 'hidden';


      btnWrapper.style.display = "block";
      console.log('Open Video');
      videoIsEnded = false;
    },

    addVideoTracking = function() {
    	Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
    		studio.video.Reporter.attach('video_1', vid1);
    	});

    	vid1MuteBtn.style.visibility = 'visible';
    	vid1UnmuteBtn.style.visibility = 'hidden';
    	vid1PlayBtn.style.visibility = 'visible';
    	vid1PauseBtn.style.visibility = 'hidden';

    	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPod/i)) ){
      	document.getElementById("sound_on").style.opacity=0;
      	document.getElementById("sound_off").style.opacity=0;
      }
    },

    closeVideoHandler = function(){
        vid1Container.style.visibility = 'hidden';
        //vid1ClickToPlay.style.visibility = 'visible';
        btnWrapper.style.display = "none";
        interfaceBg.style.visibility = 'hidden';
        vid1ClickToPlay.style.visibility = 'visible';

        vid1.currentTime = 0;
        vid1.pause();
        videoIsEnded = true;
        console.log('Close Video');

    },

    playvideoHandler = function(e){
    	vid1.play();
    	if (vid1.volume == 1.0) {
    		//vid1.volume = 1.0;
    		vid1MuteBtn.style.visibility = 'visible';
        vid1UnmuteBtn.style.visibility = 'hidden';

    	}
      else if(vid1.volume == 0.0){
        vid1MuteBtn.style.visibility = 'hidden';
        vid1UnmuteBtn.style.visibility = 'visible';
      }
    	//show requiredbuttons
    	vid1ClickToPlay.style.visibility = 'hidden';
    	vid1PlayBtn.style.visibility = 'hidden';
    	vid1PauseBtn.style.visibility = 'visible';
      vid1StopBtn.style.visibility = 'visible';
      interfaceBg.style.visibility = 'hidden';

    },

    pausePlayHandler = function(e) {
    	vid1ClickToPlay.style.visibility = 'hidden';
    	if (vid1.paused && videoIsEnded) {
    		vid1.play();
    		vid1.volume = 1.0;
    		vid1PauseBtn.style.visibility = 'visible';
    		vid1PlayBtn.style.visibility = 'hidden';
    		vid1MuteBtn.style.visibility = 'visible';

    		videoIsEnded = false;
    	}else if(vid1.paused){
    		//If Paused then Play
    		vid1.play();
    		//Show Pause button and hide Play button
    		vid1PauseBtn.style.visibility = 'visible';
    		vid1PlayBtn.style.visibility = 'hidden';

    	}else {
    		//If not paused then Pause
    		vid1.pause();
    		//Show Play button and hide Pause button
    		vid1PauseBtn.style.visibility = 'hidden';
    		vid1PlayBtn.style.visibility = 'visible';
    	}
    },

    muteUnmuteHandler = function(e) {
    	if (vid1.volume == 0.0) {
    		vid1.volume = 1.0;
    		vid1MuteBtn.style.visibility = 'visible';
    		vid1UnmuteBtn.style.visibility = 'hidden';
    	} else {
    		vid1.volume = 0.0;
    		vid1MuteBtn.style.visibility = 'hidden';
    		vid1UnmuteBtn.style.visibility = 'visible';
    	}
    },

    replayHandler = function(e) {
    	vid1.currentTime = 0;
    	vid1.play();
    	vid1.volume = 1.0;
    	vid1PauseBtn.style.visibility = 'visible';
    	vid1MuteBtn.style.visibility = 'visible';
    	vid1ClickToPlay.style.visibility = 'hidden';
    },

    stopHandler = function (e){
    	Enabler.counter('EVENT_VIDEO_STOP');
    	vid1.currentTime = 0;
    	vid1.pause();

    	videoIsEnded = true;
    	vid1ClickToPlay.style.visibility = 'visible';
    	vid1PauseBtn.style.visibility = 'hidden';
    	vid1PlayBtn.style.visibility = 'visible';
    },

    videoEndHandler = function(e) {
    	vid1.currentTime = 0;
    	vid1.pause();
    	videoIsEnded = true;

    	vid1ClickToPlay.style.visibility = 'visible';
    	vid1PauseBtn.style.visibility = 'hidden';
    	vid1PlayBtn.style.visibility = 'visible';
    };
