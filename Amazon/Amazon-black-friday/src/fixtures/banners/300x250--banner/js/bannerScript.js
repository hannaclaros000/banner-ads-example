"use strict";

var init;

window.onload = checkDCEnabler;

function checkDCEnabler() {
    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }
}

function enablerInitHandler() {
    document.getElementById('bg-exit').addEventListener('click', bgExitHandler, false);
    init();
}

function bgExitHandler() {
    Enabler.exitOverride("clickThrough", clickTag);
}

function init () {
TweenMax.set('#frame1', {x:0});
TweenMax.set('#frame2', {x:300});
TweenMax.set('#frame3', {x:300});
TweenMax.set('#smiles', {x:102, y:114, alpha: 0, scaleX: 0, scaleY: 0});
TweenMax.set('#covering-div', {alpha:0, onComplete: animation});
}

function animation() {
	slides();
	smiles();
}

function slides() {
var tl = new TimelineMax(),
	duration = 1,
	delay = 3.5;

	tl.to("#frame1, #smiles", duration, {x:"-=300", delay: delay})
	  .to("#frame2", duration, {x:0}, "-=1")
	  .to("#frame2", duration, {x:"-300", delay: delay})
	  .to("#frame3", duration, {x:0}, "-=1");
}

function smiles() {
	TweenMax.to("#smiles", 0.1, {opacity:0.5, delay:0.9});
    TweenMax.to("#smiles", 0.25, {opacity:1, scaleY:1, delay:1, ease:Quad.easeOut});
    TweenMax.to("#smiles", 0.4, {scaleX:1, delay:1, ease:Quad.easeOut});
}

