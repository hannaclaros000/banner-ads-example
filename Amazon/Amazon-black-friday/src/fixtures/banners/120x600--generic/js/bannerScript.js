"use strict";

var init;

window.onload = checkDCEnabler;

function checkDCEnabler() {
    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }
}

function enablerInitHandler() {
    document.getElementById('bg-exit').addEventListener('click', bgExitHandler, false);
    init();
}

function bgExitHandler() {
    Enabler.exitOverride("clickThrough", clickTag);
}

function init () {
    TweenMax.set("#frame1, #logo01, #logo02, #frame2, #frame3, #frame4, #frame5, #cta", {alpha: 0});
    TweenMax.set('#covering-div', {alpha:0, onComplete: animation});
}

function animation() {
    var tl = new TimelineMax(),
    duration = 0.5,
    hold = 1;

    tl.to("#logo01", duration, {alpha: 1, delay: hold})
      .to("#frame5", duration, {alpha: 1, delay: hold})
      .to("#logo02", duration, {alpha: 1, delay: hold})
      .to("#cta", duration, {alpha: 1, delay: hold})
}

