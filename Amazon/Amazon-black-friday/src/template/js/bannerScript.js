"use strict";

var init;

window.onload = checkDCEnabler;

function checkDCEnabler() {
    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }
}

function enablerInitHandler() {
    document.getElementById('bg-exit').addEventListener('click', bgExitHandler, false);
    init();
}

function bgExitHandler() {
    Enabler.exitOverride("clickThrough", clickTag);
}

var fadeIn = 1.5, fadeOut = 1 , hold = 3.5;

  function init () {
    TweenLite.to('#frame1',0,{x:300});
    TweenLite.to('#logo1', 0,{scale:.9, y:-20});
    TweenLite.to('.cta', 0,{scale:.9, y:-5});
    TweenLite.to('.terms', 0,{scale:.9, y:10});
    TweenLite.to('#frame2',0,{x:300});
    TweenLite.to('#frame3',0,{x:300});
    TweenLite.to('#frame4',0,{x:300});
    TweenLite.to('#frame5',0,{x:300});
    TweenLite.to('#frame6',0,{x:300});
    TweenLite.to('#frame7',0,{x:300});
    TweenLite.to('#frame8',0,{x:300});
    TweenLite.to('#covering-div',0,{alpha:1});
    text1();
  }

  function text1(){
    TweenLite.to('#covering-div',0,{alpha:0});
    TweenLite.to('#frame1',0,{x:0, onComplete: text2});

  }

  function text2(){
    TweenLite.to('#frame1',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame2',fadeOut,{ delay:hold,x:0, onComplete: text3});

  }

  function text3(){
    TweenLite.to('#frame2',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame3',fadeOut,{ delay:hold,x:0, onComplete: text4});
  }

  function text4(){
    TweenLite.to('#frame3',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame4',fadeOut,{ delay:hold,x:0, onComplete: text5});
  }

  function text5(){
    TweenLite.to('#frame4',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame5',fadeOut,{ delay:hold,x:0, onComplete: text6});

  }

  function text6(){
    TweenLite.to('#frame5',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame6',fadeOut,{ delay:hold,x:0, onComplete: text7});

  }

  function text7(){
    TweenLite.to('#frame6',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame7',fadeOut,{ delay:hold,x:0, onComplete: text8});
  }

  function text8(){
    TweenLite.to('#frame7',fadeOut,{delay:hold, x:-300}),
    TweenLite.to('#frame8',fadeOut,{ delay:hold,x:0});

  }
